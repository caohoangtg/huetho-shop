import React from 'react';
import logo from './logo.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h5>TRANG WEB SẼ ĐƯỢC RA MẮT SỚM<br />💖 Cám ơn bạn đã ghé thăm 💖</h5>
        <a
          className="App-link"
          href="https://www.facebook.com/huetho.234"
          target="_blank"
          rel="noopener noreferrer"
        >
          Facebook
        </a>
      </header>
    </div>
  );
}

export default App;